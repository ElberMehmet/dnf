# DNF Projesi
DNF Projesi okullarda kullanılan sınıf defterinin yerini alabilen fonksiyonlu bir websitesidir. Proje yıllık ödenen sınıf defteri giderlerinde kâra geçmeyi, kağıt tüketimini azaltmayı ve okullarda alınan yoklama sistemini hızlandırmayı hedefler. "DNF"nin açılımı "DNF's not f@tih"dir ve anlamı kendi içinde tekrar eder.

# Özellikleri
Proje 4 ana bölümden oluşur; 
- Ders öğretmeni 
- Sınıf öğretmeni 
- Nöbetçi öğretmen 
- Yetkili

Her bölüm için özellikler ve kısıtlamalar vardır. Örneğin; Ders öğretmeni yetkili kadar erişime sahip değildir.

## Ders Öğretmeni

Ders öğretmeninin kontrol edebildiği bölümdür. Bu bölümde öğretmen sadece girdiği sınıfın ders yoklamasını alabilir, değiştirebilir ya da silebilir. 
>Ayrıca burda kimin hangi derse gelip gelmediğini de öğrenebilir.

## Sınıf Öğretmeni

Sınıf öğretmeni dilediği zaman kendi sınıfının listesine bakabilir. Burada kimlerin hangi saatlerde olup olmadığını görebilir ya da yoklamayı alabilir. 
>Ayrıca uzun süreli devamsızlık yapan öğrencileri buradan tespit edebilir.

## Nöbetçi Öğretmen

Nöbetçi öğretmen, __nöbetçi olduğu gün boyunca__ bütün sınıfların; 
- Yoklamalara erişim sağlayabilir değiştirebilir ya da yoklamayı alabilir.

## Yetkili

Yetkili bölümüne sadece belirli kişiler erişebilir; **Okul Müdürü** ve **Müdür Yardımcısı**. 
Buradan;  
- Herhangi bir sınıfın yoklamasını alabilir, değiştirebilir, silebilir ve kayıt arşivini indirebilirler.
- Diğer bölümlerden giriş yapmış kullanıcıları görebilir ve işlem kayıt geçmişine erişebilirler.

# Avantajları

Normal sınıf defterine göre **daha fazla özellik** ve **kullanım imkanı** sunar.
- Yıl sonunda bütün sınıflara yeniden sınıf defteri almanız gerekmez. 
- Her eğitim öğretim yılı sonunda kayıtlar sıfırlanır. Böylece kağıttan tasarruf etmenizi sağlar. 
- Websitesi olması ayrıca teknolojiye teşvik eder.


